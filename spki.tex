\documentclass[aspectratio=169]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{tikz}
\usepackage{tikz-qtree}
\usetikzlibrary{
	arrows,
	decorations.markings,
	backgrounds,
	calc,
	fit,
	positioning,
	shapes.misc,
	shadows,
	shapes.arrows,
	shapes,
	snakes,
}

\usetheme{sigsum}

\title{SPKI intro}
\author{Niels Möller}
\date{2023-02-15}

\begin{document}
\begin{frame}
  \titlepage
  \tableofcontents
\end{frame}

\section{Setting}

\begin{frame}
  \frametitle{History}
  \begin{itemize}
  \item Late 1990s: \textsc{sha1}, \textsc{ssh}-2, \textsc{ssl}-3.0 $\rightarrow$ \textsc{tls}-1.0.
    \pause
  \item IETF adopting x.509 and ASN.1.
    \pause
  \item People: Carl Ellison, Ron Rivest, Tatu Ylonen.
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Mission}
  \begin{itemize}
  \item ``Main purpose is authorization rather than authentication''
    \pause
  \item Rethink naming.
  \item Single authorization machine.
  \item Support conversion of alien certificates.
    \pause
  \item Go read \url{https://www.rfc-editor.org/rfc/rfc2693}.
  \end{itemize}
\end{frame}

\section{Names}

\begin{frame}
  \frametitle{Global names}
  \begin{itemize}
  \item Reject CAs and global naming authorities.
  \item Pubkeys are unique ids of key holders.
  \end{itemize}
  \pause
  \begin{block}{Example}
    \texttt{(public-key (rsa-pkcs1 (e \#11\#) (n |\ldots|)))\\
      (hash sha1 |\ldots|)}
  \end{block}
\end{frame}
\begin{frame}
  \frametitle{Local names}
  \begin{itemize}
  \item Only make sense to the one using it.
  \item E.g., local alias mapping name to key.
  \end{itemize}
  \pause
  \begin{block}{Example}
    \texttt{(name linus) ; Needs context}
  \end{block}
\end{frame}
\begin{frame}
  \frametitle{Name chaining}
  \begin{block}{The thing linus calls ``poc''}
    \texttt{(name linus poc)}
  \end{block}
  \pause
  \begin{block}{Attaching namespace}
    \texttt{(name (hash sha1 |\ldots|) linus)\\
      (name (hash sha1 |\ldots|) linus poc)}
  \end{block}
\end{frame}
\begin{frame}
  \frametitle{Name certs}
  \begin{block}{To use names in authorization}
    \texttt{(cert\\
      ~(issuer (name\\
      ~~(hash sha1 |TLCgPLFlGTzgUbcaYLW8kGTEnUk=|)\\
      ~~linus)\\
      ~(subject (hash sha1 \ldots)))) ; linus' key hash\\
    (signature \ldots) ; Of the above, by the issuer key\\
    (cert\\
      ~(issuer (name (hash sha1 |linus' key hash|) poc))\\
      ~(subject (hash sha1 \ldots)))\\
    (signature \ldots) ; Of the above, by linus' key)
}
\end{block}
\end{frame}

\section{Authorization}

\begin{frame}
  \frametitle{Authorization tags}
  \begin{itemize}
  \item S-exp form.
  \item Meaning is application specific.
  \item Extending expression narrows access.
    \pause
  \item A few special \texttt{(* \ldots)} wild cards.
  \item Universal machine for intersecting.    
  \end{itemize}
  \pause
  \begin{block}{Examples}
    \texttt{(*)  ; anything\\
      (sftp (host poc.sigsum.org)) ; general access to host\\
      (sftp (host poc.sigsum.org) (read)) ; read-only\\
      (sftp (host poc.sigsum.org) (read) (* prefix /pub/))\\
      (sftp (host poc.sigsum.org)\\
      ~(* set \\
      ~~(read)\\
      ~~(*) (* prefix /home/nisse/))))}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Authorization certs}
  \begin{block}{Example}
    \texttt{(cert\\ 
      ~(issuer (hash sha1 \ldots))\\
      ~(subject (name linus))   ; in the issuer's name space\\
      ~(propagate)              ; allow delegation\\
      ~(tag (sftp (host box.sigsum.org)\\
      ~~((*) (* prefix /home/linus)))))}
  \end{block}
  \pause
  \begin{block}{Delegation}
    \texttt{(cert\\
      ~(issuer (hash sha1 |\dots|)); linus' key hash\\
      ~(subject (name rgdd))    ; in linus' name space\\
      ~(tag (sftp (host box.sigsum.org)\\
      ~~((read) /home/linus/.plan))))}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{SPKI ACLs}
  A cert without issuer, in local trusted storage.
  \begin{itemize}
  \item Resource owner creates a key pair.
  \item Configures an ACL giving that key unlimited access.
  \item Use key to delegate subset of access to other users or
    groups.
  \item Users must prove they have access.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Authorization machine}
  \begin{block}{Local input}
    ACL, requested access (key, auth tag).
  \end{block}
  \pause
  \begin{block}{From prover}
    \texttt{(sequence\\
      ~(public-key \ldots)\\
      ~(cert \ldots)\\
      ~(signature \ldots)\\
      ~$\cdots$)}
  \end{block}
  \pause
  \begin{block}{Application's responsibility}
    \begin{itemize}
    \item Get proof of possession of the private key.
    \item Interpret and enforce resulting authorization s-exp.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Revalidation}
  \begin{block}{In the cert}
    \texttt{(online reval\\
      ~(uri example.foo.com/reval?id=xxx)\\
      ~(hash sha1 |\ldots|)) ; key of validity authority}
  \end{block}
  \begin{itemize}
  \item The uri responds with signed validity statement (unless cert
    is revoked).
  \item The validity statement has a shorter lifetime than the cert
    itself.
  \item The prover fetches the statement and includes it in the proof
    sequence.
  \end{itemize}
\end{frame}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
